# -*- coding: utf-8 -*-

from odoo import  api, models, fields, _


##表1银行流水：日期，收支，类别（收款类别，付款类别），金额，付款账号，收款账号，收款人，备注

class my_account(models.Model):
    _name = 'my.account'
   
    date = fields.Datetime(u'日期',  default=fields.datetime.now(),required=True )
    bank_name = fields.Selection([('CMB_Personal',u'招行个人-8899'),('HSBC_Company',u'汇丰银行'),('ABC_Personal',u'农行个人-5614'),('ABC_Company',u'农行-公司'),('CMB_Company',u'招行-公司'),('PingAn_Company',u'平安-公司')],u'银行',required=True)
    type = fields.Selection([('cost',u'采购货款'),('collection',u'订单收款'),('expense',u'营业费用'),('management',u'管理财务费用'),('others',u'其他杂费'),('othercollection',u'其他收款')], u'类别',required=True)
    currency = fields.Selection([('USD',u'美元'),('CNY',u'人民币')], u'货币', required=True)
    amount = fields.Float(u'金额', digits=(2,3),required=True)
    suppliers = fields.Many2one('res.users',u'往来单位',required=True)
    recieve_no= fields.Char(u'收款账号', size=32)
    recieve_uid = fields.Char(u'收款人',size=16)
    note =fields.Text(u'备注')

  


    @api.one
    def set_done(self):
        self.write({'state':'done'})




    ##添加其他字段
