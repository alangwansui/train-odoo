# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'my_account',
    'version': '1.0',
    'summary': 'my_account',
    'category': 'tools',
    'author': '<jon alangwansui@gmail.com>',
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',


        'views/my_account.xml',


    ],
    'depends' : ['base', 'web',],
    'price': 20,
    'installable': True,
}



