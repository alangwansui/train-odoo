# -*- coding: utf-8 -*-

from odoo import  api, models, fields, _


##表1银行流水：日期，收支，类别（收款类别，付款类别），金额，付款账号，收款账号，收款人，备注

class my_account(models.Model):
    _name = 'my.account'

    name = fields.Char(u'名字')
    date = fields.Datetime(u'日期',  default=fields.datetime.now() )
    type = fields.Selection([('in',u'入',),('out',u'出')], u'类别')
    amount = fields.Float(u'金额', digits=(2,3))
    pay_no = fields.Char(u'付款账号', size=32)
    recieve_no= fields.Char(u'收款账号', size=32)
    recieve_uid = fields.Many2one('res.users', u'收款人')
    note=fields.Text(u'备注')

    state = fields.Selection([('draft',u'草稿'),('done',u'完成')], default='draft')


    @api.one
    def set_done(self):
        self.write({'state':'done'})




    ##添加其他字段
