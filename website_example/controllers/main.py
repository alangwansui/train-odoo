# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import sys, os
import jinja2
import logging
from odoo import http
from odoo.api import call_kw, Environment
logger = logging.getLogger(__name__)
if hasattr(sys, 'frozen'):
    # When running on compiled windows binary, we don't have access to package loader.
    path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'views'))
    loader = jinja2.FileSystemLoader(path)
else:
    loader = jinja2.PackageLoader('odoo.addons.website_example', "views")
env = jinja2.Environment(loader=loader, autoescape=True)

#------------------------------------------------------
class WebsiteBinary(http.Controller):
    @http.route(['/hello_world',], type='http', auth="public", website=False, multilang=False)
    def hello_world(self,  **kw):
        return "<h1>Hello Word</h1>"

    @http.route(['/map',], type='http', auth="public", website=False, multilang=False)
    def hello_world(self,  **kw):
        data = {'point': [116.404, 60]}
        return env.get_template("hellow_word.html").render(data)


